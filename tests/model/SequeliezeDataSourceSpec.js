'use strict';

var _ = require('lodash');
var chai = require('chai');
var ioc = require('nan-ioc');
var sinon = require('sinon');
var SequelizeDataSource = require('../../lib/model/SequelizeDataSource');
var sequelizeDataSource = new SequelizeDataSource({
  logger: console.log,
  options: {
    logging: true
  }
});
chai.should();

var stub = sinon.stub().returns(true);
var sequelize = {
  define: stub
};
_.assign(sequelizeDataSource,{
  _sequelize: sequelize
});

describe('Sequelize Data Source: \n', function(){
  describe('Define: \n',function(){
    it('Should delegate to the Sequelize method',function(){
      sequelizeDataSource.define({
        modelName: 'databaseModel',
        attributes: {},
        options: {
          tableName: 'database_model',
          underscored: true
        }
      }).should.equal(true);
      stub.firstCall.args[0].should.deep.equal('databaseModel');
      stub.firstCall.args[2].should.deep.equal({
        tableName: 'database_model',
        underscored: true
      });
    });
  });
});
