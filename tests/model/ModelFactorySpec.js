'use strict';

var _ = require('lodash');
var chai = require('chai');
var ioc = require('nan-ioc');
var sinon = require('sinon');
var ModelFactory = require('../../lib/model/ModelFactory');
var modelFactory = new ModelFactory();
chai.should();

var stub = sinon.stub().returns(true);
var dataSource = {
  define: stub
};
_.assign(modelFactory,{
  dataSource: dataSource
});

describe('Model Factory: \n', function(){
  describe('Create object: \n',function(){
    it('Should modify the arguments and call the dataSource method',function(){
      modelFactory.createObject({
        modelName: 'databaseModel'
      }).should.equal(true);
      stub.firstCall.args[0].options.tableName.should.deep.equal('database_model');
    });
  });
});
