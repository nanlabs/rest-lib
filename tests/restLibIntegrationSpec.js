'use strict';

var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../example/App');
chai.should();
chai.use(chaiHttp);
var ioc = require('nan-ioc');
var HelloController = require('../example/HelloController');
var HelloValidator = require('../example/HelloValidator');
var ProjectModel = require('../example/Project');
var HelloService = require('../example/HelloService');
var UserModel = require('../example/User');
var config = {
	server: {
		port: 5000
	},
	authentication: {
		secret: 'UPQnfo3DYA8RoXuQ',
		unsecuredUris: ['/auth']
	},
	env: 'dev',
	db: {
		database: 'restlib',
		username: 'restlib',
		password: 'restlib',
		sync: {
			force: true
		},
		options: {
			host: 'localhost',
			port: '3306',
			logging: console.log
		}
	},
	logger: {
		level: 'DEBUG'
	}
};
ioc.module(['../lib/core', '../lib/model'])
	.controller('helloController', {
		'class': HelloController,
		'context': '/hello',
		'message': 'Welcome to the framework!!!',
		'validator': HelloValidator,
		'helloService': ioc.ref('helloService'),
		'contextHolder': ioc.ref('contextHolder')
	})
	.service('helloService', {
		'class': HelloService,
		'contextHolder': ioc.ref('contextHolder'),
		'model': ioc.ref('Project')
	})
	.model('Project', {
		'class': ProjectModel
	})
	.model('User', {
		'class': UserModel
	})
	.build(config);
var dataSource = ioc.get('dataSource');
var expressServer = ioc.get('expressServer');
describe('Integration: \n', function() {
	var url = 'http://localhost:5000/';
	before(function(done) {
		dataSource.sync().then(function() {
			expressServer.run().then(function() {
				console.log('\n');
				done();
			});
		});
	})
	describe('Hello request', function() {
		it('Should return greeting message', function(done) {
			chai.request(url)
				.get('hello/')
				.query({name: 'Juan'})
				.end(function(err, res) {
					res.body.should.have.property('message').and.equal('Welcome to the framework!!! Juan');
					res.should.have.property('status').and.equal(200);
					done();
				});
		});
		it('Should return a bad request status', function(done) {
			chai.request(url)
				.get('hello/')
				.end(function(err, res) {
					res.should.have.property('status').and.equal(400);
					done();
				});
		});
	});
	describe('User create request', function() {
		it('Should return the user data', function(done) {
			chai.request(url)
				.post('hello/')
				.send({name:'Juan'})
				.end(function(err, res) {
					res.body.should.have.property('id');
					res.should.have.property('status').and.equal(200);
					done();
				});
		});
		it('Should return a bad request status', function(done) {
			chai.request(url)
				.post('hello/')
				.end(function(err, res) {
					res.should.have.property('status').and.equal(400);
					done();
				});
		});
	});
});
