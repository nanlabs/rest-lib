'use strict';

var chai = require('chai');
var ioc = require('nan-ioc');
var sinon = require('sinon');
var BodyParser = require('../../lib/core/middlewares/BodyParser');
chai.should();
var options = {
  scope: 'app',
  type: 'PRE',
  order: 10,
  options: {}
};
var JsonMiddleware = new BodyParser.JsonMiddleware(options);
var UrlEncodedMiddleware = new BodyParser.UrlEncodedMiddleware(options);
describe('Body Parser: \n', function(){
  describe('\n',function(){
    it('Should create two middlewares',function(){
      JsonMiddleware.get(options).should.be.an('function');
      UrlEncodedMiddleware.get(options).should.be.an('function');
    });
  });
});
