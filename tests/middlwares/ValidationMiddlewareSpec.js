'use strict';

var chai = require('chai');
var sinon = require('sinon');
var Joi = require('joi');
var ValidationMiddleware = require('../../lib/core/middlewares/ValidationMiddleware');
var validationMiddleware = new ValidationMiddleware();
chai.should();
var stub = sinon.stub().returns(true);

var controller = {
  validator: {
    get: {
      body: {
        name: Joi.string()
      }
    }
  }
};
var request = {
  body: {
    name: 'Jon'
  }
};

describe('Validation Middlware: \n', function(){
  describe('On Get: \n',function(){
    it('Should return the validation function',function(){
      var validateRequest = validationMiddleware.get(controller, 'get');
      validateRequest(request,null,stub).should.equal(true);
      stub.firstCall.should.not.be.null;
    });
  });
});
