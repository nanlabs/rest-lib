'use strict';

var chai = require('chai');
var sinon = require('sinon');
var Joi = require('joi');
var RequestProcessorMiddleware = require('../../lib/core/middlewares/RequestProcessorMiddleware');
var requestProcessorMiddleware = new RequestProcessorMiddleware();
chai.should();
var stub = sinon.stub().returns(true);

var controller = {
  validator: {
    get: {
      body: {
        name: Joi.string()
      }
    }
  }
};
var request = {
  body: {
    name: 'Jon'
  }
};

describe('Request Processor Middlware: \n', function(){
  describe('On Get: \n',function(){
    it('Should return the processor function',function(){
      var requestProcessor = requestProcessorMiddleware.get(controller, 'get');
      requestProcessor(request,null,stub);
      stub.firstCall.should.not.be.null;
      request.should.have.property('command').and.be.an('object').and.have.property('name');
    });
  });
});
