'use strict';

var chai = require('chai');
var ioc = require('nan-ioc');
var sinon = require('sinon');
var ExpressMiddlewareWrapper = require('../../lib/core/middlewares/ExpressMiddlewareWrapper');
chai.should();

var stub = sinon.stub().returns(true);
var middleware = {
  build: stub
};
var options = {
  scope: 'app',
  type: 'PRE',
  order: 10
};

describe('Express Middleware Wrapper: \n', function(){
  describe('Wrap: \n',function(){
    it('Should return the wrapped function',function(){
      var WrappedBuilder = ExpressMiddlewareWrapper.wrap(middleware);
      var wrappedMiddleware = new WrappedBuilder(options);
      wrappedMiddleware.get();
      stub.firstCall.args[0].should.deep.equal(options);
    });
  });
});
