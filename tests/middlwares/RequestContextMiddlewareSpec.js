'use strict';

var _ = require('lodash');
var chai = require('chai');
var ioc = require('nan-ioc');
var sinon = require('sinon');
var RequestContextMiddleware = require('../../lib/core/middlewares/RequestContextMiddleware');
chai.should();
var pre = new RequestContextMiddleware.pre();
var post = new RequestContextMiddleware.post();
var stub = sinon.stub().returns(true);
var contextHolder = {
  init: stub,
  end: stub
};
_.assign(pre,{
  contextHolder: contextHolder
});
_.assign(post,{
  contextHolder: contextHolder
});
describe('Request Context Middleware: \n', function(){
  describe('Get: \n',function(){
    it('Should return the context function',function(){
      var preF = pre.get();
      var postF = post.get();
      preF(null,null,stub);
      postF(null,null,stub);
      stub.firstCall.should.not.be.null;
      stub.secondCall.should.not.be.null;
      stub.thirdCall.should.not.be.null;
      stub.lastCall.should.not.be.null;
    });
  });
});
