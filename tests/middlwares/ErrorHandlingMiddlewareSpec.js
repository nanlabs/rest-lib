'use strict';

var _ = require('lodash');
var chai = require('chai');
var sinon = require('sinon');
var Joi = require('joi');
var ErrorHandlingMiddleware = require('../../lib/core/middlewares/ErrorHandlingMiddleware');
var errorHandlingMiddleware = new ErrorHandlingMiddleware();
chai.should();
var stub = sinon.stub().returns(true);
var response = {
  set: stub,
  status: stub,
  json: stub
};
var err = new Error({status: 404});
_.assign(errorHandlingMiddleware,{
  logger: {
    error: stub
  }
});
describe('Error Handling Middlware: \n', function(){
  describe('On Get: \n',function(){
    it('Should return the handler function',function(){
      var errorHandler = errorHandlingMiddleware.get();
      errorHandler(err,null,response,stub);
      stub.firstCall.should.not.be.null;
      stub.secondCall.should.not.be.null;
      stub.thirdCall.should.not.be.null;
    });
  });
});
