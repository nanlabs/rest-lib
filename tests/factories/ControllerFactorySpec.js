'use strict';

var _ = require('lodash');
var chai = require('chai');
var sinon = require('sinon');
var ControllerFactory = require('../../lib/core/factories/ControllerFactory');
var controllerFactory = new ControllerFactory();
chai.should();
var stub = sinon.stub().returns(true);

_.assign(controllerFactory,{
  createInstance: stub,
  expressServer: {
    addController: stub
  }
});

describe('Controller Factory: \n', function(){
  describe('Create Object: \n',function(){
    it('Should return the built object',function(){
      controllerFactory.createObject('class',{_mixins: false}).should.equal(true);
      stub.firstCall.args[0].should.equal('class');
    });
  });
});
