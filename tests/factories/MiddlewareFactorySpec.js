'use strict';

var _ = require('lodash');
var chai = require('chai');
var sinon = require('sinon');
var MiddlewareFactory = require('../../lib/core/factories/MiddlewareFactory');
var stub = sinon.stub().returns(true);
var expressServer = {
  addMiddleware: stub
};
var middlewareFactory = new MiddlewareFactory({expressServer: expressServer});
chai.should();

_.assign(middlewareFactory,{
  createInstance: stub
});

describe('Controller Factory: \n', function(){
  describe('Create Object: \n',function(){
    it('Should return the built object',function(){
      middlewareFactory.createObject('class',{_mixins: false}).should.equal(true);
      stub.firstCall.args[0].should.equal('class');
    });
  });
});
