'use strict';

var _ = require('lodash');
var chai = require('chai');
var sinon = require('sinon');
var ServiceFactory = require('../../lib/core/factories/ServiceFactory');
var serviceFactory = new ServiceFactory();
chai.should();
var stub = sinon.stub().returns(true);

_.assign(serviceFactory,{
  createInstance: stub
});

describe('Service Factory: \n', function(){
  describe('Create Object: \n',function(){
    it('Should return the built object',function(){
      serviceFactory.createObject('class').should.equal(true);
      stub.firstCall.args[0].should.equal('class');
    });
  });
});
