/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var gulp = require('gulp');
var license = require('gulp-license');
var mocha = require('gulp-mocha');
var istanbul = require('gulp-istanbul');

gulp.task('license', function() {
  gulp.src(['./**/*.js', '!node_modules/**'])
    .pipe(license('Apache', {tiny: false, organization: 'NaN Labs'}))
    .pipe(gulp.dest('./'))
});

gulp.task('pre-test', function() {
	return gulp.src(['lib/**/**/*.js'])
		// Covering files
		.pipe(istanbul({includeUntested: true}))
		// Force `require` to return covered files
		.pipe(istanbul.hookRequire());
});

gulp.task('test', ['pre-test'], function() {
	return gulp.src(['tests/**/*.js'])
		.pipe(mocha())
		// Creating the reports after tests ran
		.pipe(istanbul.writeReports({
			dir: './coverage',
			reporters: ['lcov', 'json', 'text', 'text-summary','html'],
			reportOpts: {
				dir: './coverage'
			},
		}));
});

gulp.task('default', ['test']);
