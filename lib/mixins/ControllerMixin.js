/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var Mixin = require('nan-mixin');
var Boom = require('boom');

module.exports = Mixin.create('ControllerMixin', {

  list: function (request, response) {
    throw Boom.notImplemented();
  },

  get: function (request, response) {
    throw Boom.notImplemented();
  },

  create: function (request, response) {
    throw Boom.notImplemented();
  },

  update: function (request, response) {
    throw Boom.notImplemented();
  },

  delete: function (request, response) {
    throw Boom.notImplemented();
  }
});
