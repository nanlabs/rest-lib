/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var ioc = require('nan-ioc');
var DataSourceFactory = require('./DataSourceFactory');
var DataSource = require('./DataSource');
var ModelFactory = require('./ModelFactory');
var TransactionManager = require('./transaction/TransactionManager');
var TransactionalServiceEnhancer = require('./transaction/TransactionalServiceEnhancer');

module.exports=ioc.module(['../core'])
  .factory('datasource', {
    'class': DataSourceFactory
  })
  .factory('model', {
    'class': ModelFactory,
    'dataSource': ioc.ref('dataSource')
  })
  .datasource('dataSource', {
    'class': DataSource.Sequelize,
    'database': '${db.database}',
    'username': '${db.username}',
    'password': '${db.password}',
    'options': '${db.options}',
    'logger': ioc.ref('logger')
  })
  .component('transactionManager', {
    'class': TransactionManager,
    'contextHolder': ioc.ref('contextHolder'),
    'dataSource': ioc.ref('dataSource')
  })
  .component('transactionalServiceEnhancer', {
    'class': TransactionalServiceEnhancer,
    'factory': ioc.ref('service__Factory'),
    'transactionManager': ioc.ref('transactionManager')
  })
  .config({
    db: {
      options: {
        dialect: 'mysql',
        underscored: true,
        pool: {
          maxConnections: 5,
          maxIdleTime: 30
        },
        define: {
          timestamps: true,
          paranoid: true,
          underscored: true,
          underscoredAll: true
        }
      }
    }
  });
