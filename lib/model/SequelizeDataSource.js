/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var Sequelize = require('sequelize');
var _ = require('lodash');

module.exports = SequelizeDataSource;

/**
 *
 * @param options
 * @constructor
 */
function SequelizeDataSource(options){
  this.logger = options.logger;
  this._sync = options.sync;
  options.options.logging = function(str){
    this.logger.debug(str);
  }.bind(this);
  this._sequelize = new Sequelize(options.database, options.username, options.password, options.options);
  this._modelDefinitions = [];
}

/**
 *
 * @param definition
 * @returns {Model}
 */
SequelizeDataSource.prototype.define = function(definition){
  this._modelDefinitions.push(definition);
  return this._sequelize.define(definition.modelName, definition.attributes, definition.options);
};

/**
 *
 */
SequelizeDataSource.prototype.onInitialized = function(){

  _.forEach(this._modelDefinitions, function(modelDefinition){
    if(_.isFunction(modelDefinition.afterDefined)){
      var current = this._sequelize.model(modelDefinition.modelName);
      modelDefinition.afterDefined.bind(current)(this._sequelize.models);
    }
  }.bind(this));
};

/**
 *
 * @returns {Promise}
 */
SequelizeDataSource.prototype.sync = function(){
  return this._sequelize.sync(this._sync);
};
