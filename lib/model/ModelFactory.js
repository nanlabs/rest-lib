/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var _ = require('lodash');
module.exports = ModelFactory;

function ModelFactory(){

}

ModelFactory.prototype.createObject = function(definition){
  this.setDefaultAttributes(definition);
  return this.dataSource.define(definition);
};

ModelFactory.prototype.onInitialized = function(){

};

/**
 * Set field names as underscored by default (fieldName -> field_name)
 * @param attributes
 * @returns {{}}
 */
ModelFactory.prototype.setDefaultAttributes= function(definition){

  var patchedAttributes = {};
  _.forEach(definition.attributes, function (attributeOptions, attributeName) {

    var snakedCaseAttributeName = _.snakeCase(attributeName);
    var newAttributeOptions = attributeOptions;
    if (!_.isFunction(attributeOptions)) {
      if (!attributeOptions.field) {
        attributeOptions.field = snakedCaseAttributeName;
      }

      newAttributeOptions = attributeOptions;
    } else {
      newAttributeOptions = {type: attributeOptions, field: snakedCaseAttributeName};
    }

    patchedAttributes[attributeName] = newAttributeOptions;
  });
  definition.attributes = patchedAttributes;
  definition.options = definition.options || {};
  this.assignDefault(definition.options, 'tableName', _.snakeCase(definition.modelName));
  this.assignDefault(definition.options, 'underscored', true);
};

ModelFactory.prototype.assignDefault = function(object, key, value){
  if(object[key] === undefined){
    object[key] = value;
  }
};
