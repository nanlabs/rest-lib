/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var _ = require('lodash');
var Q = require('q');
var Sequelize = require('sequelize');

module.exports = TransactionManager;

var NAMESPACE = 'transaction_manager';

/**
 *
 * @constructor
 */
function TransactionManager(){
  this._init();
  this.manualMode = false;
  this.bindClsAdapter();
  this.contextHolder.on('ready', this.initialize.bind(this));
}

/**
 *
 * @param method
 * @param options
 * @return {*}
 */
TransactionManager.prototype.execute = function(method, options){
  var inherits, transaction, shouldManage = false;
  inherits = _.isObject(options) ? options.inherit : true;
  if(inherits){
    transaction = this.currentTransaction();
    if(!transaction){
      transaction = this.createTransaction(options);
      shouldManage = true;
    }else{
      if(!transaction.finished){
        return method();
      }else{
        //finished transactions can't be used, so we must create new one
        this.transactionStack().shift();
        transaction = this.createTransaction(options);
        shouldManage = true;
      }
    }
  }else{
    transaction = this.createTransaction(options);
    shouldManage = true;
  }
  if(this.manualMode || !shouldManage){
    return method();
  }else{
    return this.handleMethod(this.transactionStack(), transaction, method);
  }
};

/**
 *
 * @param transaction
 * @param method
 * @param manage
 * @return {*}
 */
TransactionManager.prototype.handleMethod = function(transactionStack, transaction, method){
  return Q.Promise(function(resolve, reject){
    transaction.prepareEnvironment().then(function(){
      var promise = method();
      if(!Q.isPromiseAlike(promise)){
        throw new Error('Transactional service methods must return a Promise');
      }
      promise.then(function(result){
        transaction.commit().then(function(){
          resolve(result);
        });
      }).catch(function(err){
        transaction.rollback().finally(function(){
          reject(err);
        });
      });
    });
  });
};

/**
 *
 * @return {Transaction}
 */
TransactionManager.prototype.createTransaction = function(options){
  var transaction = new Sequelize.Transaction(this.dataSource._sequelize, options);
  return transaction;
};

/**
 *
 */
TransactionManager.prototype.transactionStack = function(){
  return this.contextHolder.get(NAMESPACE);
};

/**
 *
 * @return {*}
 */
TransactionManager.prototype.currentTransaction = function(){
  var txStack = this.transactionStack();
  return txStack ? txStack[0]: undefined;
};

/**
 *
 */
TransactionManager.prototype.initialize = function(){
  this.contextHolder.set(NAMESPACE, []);
};

/**
 *
 * @param name
 * @param transaction
 */
TransactionManager.prototype.setTransaction = function(name, transaction){
  if(transaction){
    this.transactionStack().unshift(transaction);
  }else{
    this.transactionStack().shift();
  }
};

/**
 *
 */
TransactionManager.prototype.bindClsAdapter = function(){
  Sequelize.cls = {
    'get': this.currentTransaction.bind(this),
    'set': this.setTransaction.bind(this),
    bind: _.identity,
    createContext: _.noop,
    getNamespace: _.noop,
    createNamespace: _.noop
  };
};
