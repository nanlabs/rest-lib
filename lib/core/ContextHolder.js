/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var domain = require('domain');
var cuid = require('cuid');
var events = require('events');
module.exports = ContextHolder;

/**
 *
 * @constructor
 */
function ContextHolder(){
  this._init();
  this._emitter = new events.EventEmitter();
  this.on = this._emitter.on;
  this.emit = this._emitter.emit;
  this.initialized = false;
}

/**
 *
 */
ContextHolder.prototype.init = function(){
  var _domain = domain.create();
  _domain.enter();
  _domain.context = {id: cuid.slug()};
  this.initialized = true;
  this.emit('ready');
};

/**
 *
 */
ContextHolder.prototype.end = function(){
  domain.active.exit();
};

/**
 *
 * @param key
 * @return {*}
 */
ContextHolder.prototype.get = function(key){
  return this.initialized ? domain.active.context[key] : undefined;
};

/**
 *
 * @param key
 * @param value
 */
ContextHolder.prototype.set = function(key, value){
  if(!this.initialized){
    throw new Error('Illegal state. ContextHolder cannot be accessed before initialized domain')
  }
  domain.active.context[key] = value;
};
