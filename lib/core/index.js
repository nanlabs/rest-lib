/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var ioc = require('nan-ioc');
var ExpressServer = require('./ExpressServer');
var RouterBuilder = require('./RouterBuilder');
var ContextHolder = require('./ContextHolder');

module.exports = ioc.module(['./factories', './middlewares', './logger'])
  .component('expressServer', {
    'class': ExpressServer,
    'routerBuilder': ioc.ref('routerBuilder'),
    'port': '${server.port}'
  })
  .component('contextHolder', {
    'class': ContextHolder
  })
  .config({
    server:{
      port: 3000
    }
  })
  .component('routerBuilder', {
    'class': RouterBuilder
  });

