/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var path = require('path');
var util = require('util');
var moment = require('moment');

module.exports = Logger;

/**
 * ERROR LEVELS.
 * Higher Levels include the lower levels.
 */
var levels = {
  ERROR: 0,
  WARN: 1,
  INFO: 2,
  DEBUG: 3,
  TRACE: 4,
  ALL: 5
};

/**
 * Gets the source information from the line being log
 */
function getSourceInfo() {
  var exec = getStack()[3];

  var pos = exec.getFileName().lastIndexOf('\\');
  if (pos < 0) exec.getFileName().lastIndexOf('/');

  var projectRoot = path.dirname(require.main.filename) + '/';

  var filename = exec.getFileName().substring(pos + 1);
  filename = filename.replace(projectRoot, '');

  return {
    methodName: exec.getFunctionName() || 'anonymous',
    file: filename,
    lineNumber: exec.getLineNumber()
  };
};


/**
 * Gets the current stack in order to retrieve source code info.
 *
 * Based on Callsite (http://github.com/visionmedia/callsite)
 * Copyright (c) 2011, 2013 TJ Holowaychuk <tj@vision-media.ca>
 */
function getStack() {
  var old = Error.stackTraceLimit;
  Error.stackTraceLimit = 4;
  var orig = Error.prepareStackTrace;
  Error.prepareStackTrace = function(_, stack){ return stack; };
  var err = new Error;
  Error.captureStackTrace(err, arguments.callee);
  var stack = err.stack;
  Error.prepareStackTrace = orig;

  Error.stackTraceLimit = old;

  return stack;
};

/**
 * Writes the log to stderr
 *
 * Partially based on clim (http://github.com/epeli/node-clim)
 * Copyright (c) 2009-2011 Esa-Matti Suuronen <esa-matti@suuronen.org>
 */
function writeLog(level, currentLevel, context, msg) {
  if (levels[level] <= currentLevel) {
    var line;

    var si = getSourceInfo();

    if (context && context.get('id')) {
      line = util.format("[%s] <%s> (%s) [%s:%s] %s", moment().format(), level, context.get('id'), si.file, si.lineNumber, msg);
    } else {
      line = util.format("[%s] <%s> [%s:%s] %s", moment().format(), level, si.file, si.lineNumber, msg);
    }

    line += '\n';
    logToConsole(level, line);
  }
};

function logToConsole(level, line) {
  process.stderr.write(line);
};

function Logger() {
  this._init();
  console.log('Current log level: ' + this.level);
  this.currentLevel = levels[this.level];
};

Logger.prototype.info = function() {
  var msg = util.format.apply(this, arguments);
  writeLog('INFO', this.currentLevel, this.contextHolder, msg);
};

Logger.prototype.warn = function() {
  var msg = util.format.apply(this, arguments);
  writeLog('WARN', this.currentLevel, this.contextHolder, msg);
};

Logger.prototype.error = function() {
  var msg = util.format.apply(this, arguments);
  writeLog('ERROR', this.currentLevel, this.contextHolder, msg);
};

Logger.prototype.debug = function() {
  var msg = util.format.apply(this, arguments);
  writeLog('DEBUG', this.currentLevel, this.contextHolder, msg);
};
