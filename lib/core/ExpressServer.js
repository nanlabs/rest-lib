/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var express = require('express');
var _ = require('lodash');
var Q = require('q');


module.exports = ExpressServer;


/**
 *
 * @constructor
 */
function ExpressServer() {
  this.server = express();
  this.controllers = [];
  this.middlewares = [];
}

/**
 * Starts Express Server
 */
ExpressServer.prototype.run = function () {
  function setHttpServer(instance, httpServer) {
    const http = httpServer;
    instance.getHttpServer = function () {
      return http;
    }.bind(instance);
  };
  var defer = Q.defer();
  this.loadPreRouteMiddlewares();
  this.loadControllers();
  this.loadPostRouteMiddlewares();
  const httpServer = this.server.listen(this.port, function(err){
    if(err){
      defer.reject(err);
    }else{
      console.log('Server listening on port %s...', this.port);
      defer.resolve();
    }
  }.bind(this));

  setHttpServer(this, httpServer);
  
  return defer.promise;
};

/**
 *
 */
ExpressServer.prototype.loadControllers = function () {
  var pre = this.filterMiddlewares(['isPreRoute', 'isControllerScope']);
  var post = this.filterMiddlewares(['isPostRoute', 'isControllerScope']);
  _.forEach(this.controllers, function (controller) {
    this.server.use(this.routerBuilder.build(controller, pre, post));
  }.bind(this));
};


/**
 *
 */
ExpressServer.prototype.loadPreRouteMiddlewares = function () {
  this.loadAppMiddlewares(['isPreRoute', 'isAppScope']);
};

/**
 *
 */
ExpressServer.prototype.loadPostRouteMiddlewares = function () {
  this.loadAppMiddlewares(['isPostRoute', 'isAppScope']);
};

/**
 *
 * @param filters
 */
ExpressServer.prototype.loadAppMiddlewares = function (filters) {
  var middlewares = this.filterMiddlewares(filters).value();
  _.forEach(middlewares, function (middleware) {
    this.server.use(middleware.get());
  }.bind(this));
};

/**
 * Filter middlewares by <b>filters</b> criteria
 * @param filters
 * @returns {Array} array wrapped by lodash object to chain calls
 */
ExpressServer.prototype.filterMiddlewares = function (filters) {
  return _(this.middlewares)
    .filter(function (middleware) {
      return _.every(filters, function (filter) {
        return middleware[filter]();
      });
    })
    .sortBy('order');
};

/**
 *
 */
ExpressServer.prototype.onStarted = function () {
  console.log('Server listening on port %s...', this.port);
};

/**
 *
 * @param controller
 */
ExpressServer.prototype.addController = function (controller) {
  this.controllers.push(controller);
};


/**
 *
 * @param middleware
 */
ExpressServer.prototype.addMiddleware = function (middleware) {
  this.middlewares.push(middleware);
};
