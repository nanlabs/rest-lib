/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var express = require('express');
var _ = require('lodash');
var Q = require('q');
var util = require('util');

module.exports = RouterBuilder;

var ROUTES_MAP = {
  'list': {verb: 'get', route: '%s'},
  'get': {verb: 'get', route: '%s/:id'},
  'create': {verb: 'post', route: '%s'},
  'update': {verb: 'put', route: '%s/:id'},
  'delete': {verb: 'delete', route: '%s/:id'}
};

/**
 *
 * @constructor
 */
function RouterBuilder(){
}

/**
 *
 * @param controller
 * @param preRouteMiddlewares
 * @param postRouteMiddlewares
 * @returns {*}
 */
RouterBuilder.prototype.build = function (controller, preRouteMiddlewares, postRouteMiddlewares) {
  var sanitizeRoute = function(result, n, key) {
    n.route = '%s/' + n.route;
    result[key] = n;
  }
  var routes = _.assign({}, ROUTES_MAP, _.transform(controller.routes, sanitizeRoute));
  var router = express.Router();
  _.forEach(routes, function (map, method) {
    var params = [util.format(map.route, controller.context)]
      .concat(preRouteMiddlewares.map(this.mapMiddlewareByMethod(controller, method)).compact().value())
      .concat([this.buildActionWrapper(controller[method].bind(controller))])
      .concat(postRouteMiddlewares.map(this.mapMiddlewareByMethod(controller, method)).compact().value());
    router[map.verb].apply(router, params);
  }.bind(this));
  return router;
};


/**
 *
 * @param method
 * @returns {Function}
 */
RouterBuilder.prototype.mapMiddlewareByMethod = function (controller, method) {
  return function (middleware) {
    return middleware.get(controller, method);
  }
};


/**
 *
 * @param action
 * @returns {Function}
 */
RouterBuilder.prototype.buildActionWrapper = function (action) {
  return function actionWrapper(request, response, next) {
    var result = request.command ? action(request.command, request, response) : action(request, response);
    var responseHandler = function (object) {
      if(!response.headersSent){
        response.send(object);
      }
      next();
      return true;
    };
    if (!response.headersSent) {
      if (!result) {
        response.end();
      } else if (Q.isPromiseAlike(result)) {
        result.then(responseHandler).then(undefined, next);
      } else {
        responseHandler(result);
      }
    }
  };
};
