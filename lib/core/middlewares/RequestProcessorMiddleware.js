/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var _ = require('lodash');

module.exports = RequestProcessorMiddleware;

/**
 *
 * @constructor
 */
function RequestProcessorMiddleware(){
}

/**
 *
 * @param controller
 * @param method
 * @returns {*}
 */
RequestProcessorMiddleware.prototype.get = function (controller, method) {
  if (controller.validator && controller.validator[method]) {
    return this.build(controller.validator[method]);
  } else {
    return undefined;
  }
};

/**
 *
 * @param schema
 * @param options
 * @returns {Function}
 */
RequestProcessorMiddleware.prototype.build = function (schema) {

  return function validateRequest(req, res, next) {
    var commandObject = {};
    if (!schema) {
      return next();
    }
    if (schema.map) {
      // Use the map definition to create the command object
      _.each(schema.map, function (requestKey, commandKey) {
        commandObject[commandKey] = _.get(req, requestKey);
      });
    } else {
      ['params', 'body', 'query', 'headers'].forEach(function (key) {
        if (schema[key]) {
          var schemaObject = schema[key];
          _.forEach(schemaObject, function (val, schemaKey) {
            commandObject[schemaKey] = req[key][schemaKey];
          });
        }
      });
    }
    req.command = commandObject;
    next();
  };
};
