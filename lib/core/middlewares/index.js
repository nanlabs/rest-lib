/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var ioc = require('nan-ioc');
var BodyParser = require('./BodyParser');
var ValidationMiddleware = require('./ValidationMiddleware');
var ErrorHandlingMiddleware = require('./ErrorHandlingMiddleware');
var RequestProcessorMiddleware = require('./RequestProcessorMiddleware');
var RequestContextMiddleware = require('./RequestContextMiddleware');

module.exports = ioc.module(['../factories'])
  .middleware('preRequestContextMiddleware', {
    'class': RequestContextMiddleware.pre,
    'contextHolder': ioc.ref('contextHolder'),
    'scope': 'application',
    'type': 'pre',
    'order': 2
  })
  .middleware('jsonMiddleware', {
    'class': BodyParser.JsonMiddleware,
    'scope': 'application',
    'type': 'pre',
    'order': 1,
    'options': '${bodyParser.json}'
  })
  .middleware('urlEncodedMiddleware', {
    'class': BodyParser.UrlEncodedMiddleware,
    'scope': 'application',
    'type': 'pre',
    'order': 0,
    'options': '${bodyParser.urlencoded}'
  })
  .middleware('errorHandlingMiddleware', {
    'class': ErrorHandlingMiddleware,
    'scope': 'application',
    'type': 'post',
    'order': 20
  })
  .middleware('postRequestContextMiddleware', {
    'class': RequestContextMiddleware.post,
    'contextHolder': ioc.ref('contextHolder'),
    'scope': 'application',
    'type': 'post',
    'order': 1000
  })
  .middleware('validationMiddleware', {
    'class': ValidationMiddleware,
    'scope': 'controller',
    'type': 'pre',
    'order': 10
  })
  .middleware('requestProcessorMiddleware', {
    'class': RequestProcessorMiddleware,
    'scope': 'controller',
    'type': 'pre',
    'order': 20
  })
  .config({
    bodyParser: {
      json: {
        limit: '5mb'
      },
      urlencoded: {
        extended: false,
        limig: '5mb'
      }
    }
  });
