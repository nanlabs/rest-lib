/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var _ = require('lodash');

/**
 * Helper module to wrap an ExpressJs middleware into a rest-lib Middleware
 * @module ExpressMiddlewareWrapper
 */
module.exports = {
  wrap: wrap
};

/**
 * Creates a new Middleware Class based on <b>expressMiddleware</b> closure
 * @param expressMiddleware
 * @returns {Function}
 */
function wrap(expressMiddleware) {
  var builder = _.isFunction(expressMiddleware) ? createBuilder(expressMiddleware) : expressMiddleware;
  return getClass(builder);
}

/**
 * @private
 * @param expressMiddleware
 * @returns {{build: Function}}
 */
function createBuilder(expressMiddleware) {
  return {
    build: function () {
      return expressMiddleware;
    }
  }
}

/**
 * @private
 * @param builder
 * @returns {Function}
 */
function getClass(builder) {
  return function (options) {
    this.scope = options.scope;
    this.type = options.type;
    this.order = options.order;
    this.get = function () {
      return builder.build(options);
    }
  };
}
