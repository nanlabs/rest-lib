/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var Joi = require('joi');
var Boom = require('boom');
var _ = require('lodash');

module.exports = ValidationMiddleware;

/**
 *
 * @constructor
 */
function ValidationMiddleware() {
}

/**
 *
 * @param controller
 * @param method
 * @returns {*}
 */
ValidationMiddleware.prototype.get = function (controller, method) {
  if (controller.validator && controller.validator[method]) {
    return this.build(controller.validator[method]);
  } else {
    return undefined;
  }
};

/**
 *
 * @param schema
 * @param options
 * @returns {Function}
 */
ValidationMiddleware.prototype.build = function (schema) {
  var options = { abortEarly: false };

  var self = this;

  return function validateRequest(req, res, next) {
    var toValidate = {};

    if (!schema) {
      return next();
    }

    ['params', 'body', 'query', 'headers'].forEach(function (key) {
      if (schema[key]) {
        toValidate[key] = req[key];
      }
    });

    return Joi.validate(toValidate, schema, options, onValidationComplete);

    function onValidationComplete(err, validated) {
      if (err) {
        var extractedDetails = _.map(err.details, function (errorDetail) {
          return {
            attribute: errorDetail.path,
            type: errorDetail.type
          };
        });

        self.logger.debug('Validation error in controller ' + req.originalUrl + ', details: ', extractedDetails);

        return next(Boom.badRequest('Bad Request', extractedDetails));
      }
      // copy the validated data to the req object
      _.extend(req, validated);
      return next();
    }
  };
};
