/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var _ = require('lodash');
var Boom = require('boom');

module.exports = ErrorHandlingMiddleware;

function ErrorHandlingMiddleware () {
};

ErrorHandlingMiddleware.prototype.get = function () {

  var self = this;

  return function (err, req, res, next) {
    if (!err.isBoom) {
      var status;
      if (!(err instanceof Error)) {
        err = new Error(err);
      }
      if(err.name !== 'UnauthorizedError'){
        self.logger.error(err.stack);
      }
      status = err.statusCode || err.status;
      if(status == 401){
        err.message = 'Authorization required';
      }
      Boom.wrap(err, status);
      err.reformat();
    } else {
      self.logger.debug('Error details:', err);
    }

    var responseObject = _.omit(err.output.payload, ['statusCode', 'error']);
    if (err.data) {
      responseObject = _.extend(responseObject, {details: err.data});
    }

    if(!res.headersSent){
      res.set(err.output.headers);
      res.status(err.output.statusCode);
      return res.json(responseObject);
    }
    else{
      return next();
    }
  };
};
