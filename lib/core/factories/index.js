/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var ioc = require('nan-ioc');
var MiddlewareFactory = require('./MiddlewareFactory');
var ControllerFactory = require('./ControllerFactory');
var ServiceFactory = require('./ServiceFactory');
module.exports= ioc.module(['../logger'])
  .factory('middleware', {
    'class': MiddlewareFactory,
    'expressServer': ioc.ref('expressServer'),
    'logger': ioc.ref('logger')
  })
  .factory('controller', {
    'class': ControllerFactory,
    'expressServer': ioc.ref('expressServer'),
    'logger': ioc.ref('logger')
  })
  .factory('service', {
    'class': ServiceFactory,
    'logger': ioc.ref('logger')
  });
