/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var MiddlewareMixin = require('../../mixins/MiddlewareMixin');
module.exports = MiddlewareFactory;

/**
 *
 * @constructor
 */
function MiddlewareFactory(options) {
  this.expressServer = options.expressServer;
}

/**
 *
 * @param Class
 * @param params
 * @returns {*}
 */
MiddlewareFactory.prototype.createObject = function (Class, params) {
  params._mixins = params._mixins || [MiddlewareMixin];
  var middleware = this.createInstance(Class, params);
  middleware.logger = this.logger;
  this.expressServer.addMiddleware(middleware);
  return middleware;
};


