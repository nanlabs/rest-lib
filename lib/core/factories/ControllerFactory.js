/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var ControllerMixin = require('../../mixins/ControllerMixin');

module.exports = ControllerFactory;

/**
 *
 * @constructor
 */
function ControllerFactory(options) {
}

/**
 *
 * @param Class
 * @param params
 * @returns {*}
 */
ControllerFactory.prototype.createObject = function (Class, params) {
  params._mixins = params._mixins || [ControllerMixin];
  var controller = this.createInstance(Class, params);
  controller.logger = this.logger;
  this.expressServer.addController(controller);
  return controller;
};


