/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var ioc = require('nan-ioc');
var HelloController = require('./HelloController');
var HelloValidator = require('./HelloValidator');
var ProjectModel = require('./Project');
var HelloService = require('./HelloService');
var UserModel = require('./User');

var config = {
  server: {
    port: 5000
  },
  authentication: {
    secret: 'UPQnfo3DYA8RoXuQ',
    unsecuredUris: ['/auth']
  },
  env: 'dev',
  db: {
    database: 'restlib',
    username: 'restlib',
    password: 'restlib',
    sync: {force: true},//see http://docs.sequelizejs.com/en/latest/api/sequelize/#syncoptions-promise
    options: {
      host: 'localhost',
      port: '3306',
      logging: console.log
    }
  },
  logger: {
    level: 'debug'
  }
};
ioc.module(['../lib/core', '../lib/model'])
  .controller('helloController', {
    'class': HelloController,
    'context': '/hello',
    'message': 'Welcome to the framework!!!',
    'validator': HelloValidator,
    'helloService': ioc.ref('helloService'),
    'contextHolder': ioc.ref('contextHolder')
  })
  .service('helloService', {
    'class': HelloService,
    'contextHolder': ioc.ref('contextHolder'),
    'model': ioc.ref('Project')
  })
  .model('Project', {
    'class': ProjectModel
  })
  .model('User', {
    'class': UserModel
  })
  .build(config);

var dataSource = ioc.get('dataSource');
var expressServer = ioc.get('expressServer');
dataSource.sync().then(function(){
  expressServer.run().then(function(){
    console.log('running');
  });
});


