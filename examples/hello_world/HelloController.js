/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

module.exports = HelloController;
var i = 0;
function HelloController() {
}

HelloController.prototype.list = function (command) {
  return {
    'message': this.message + ' ' +  command.name
  };
};

HelloController.prototype.create = function (command) {
  console.log(this.contextHolder.get('id'),' < ', ++i, ' > receiving request command: ', command);
  return this.helloService.create(command);
};




