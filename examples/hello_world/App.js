/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var Q = require('q');
module.exports = App;

function App(){
  this._init();
  var bootstrap = [];
  if(this.env === 'dev'){
    bootstrap.push(this.syncDb.bind(this));
  }
  bootstrap.push(this.loadData.bind(this));
  bootstrap.reduce(Q.when, Q(true));
}

App.prototype.syncDb = function(){
  return this.dataSource.sync();
};

App.prototype.loadData = function(){
  var user,
    project,
    User = this.User,
    Project = this.Project;
    return User.create({
      firstName: 'John',
      lastName: 'Hancock'
    }).then(function(userModel){
      user = userModel;
      return Project.create({
        name: 'Mila'
      });
    }).then(function(projectModel){
      project = projectModel;
      return user.addProject(project);
    }).then(function(){
      console.log('Created project: ', project.get());
      console.log('Created user: ', user.get());
    });
};

