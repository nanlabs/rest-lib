/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var ioc = require('nan-ioc');
var UserModel = require('./User');
var ProjectModel = require('./Project');
var App = require('./App');


ioc.module(['../lib/model'])
  .model('User', {
    'class': UserModel
  })
  .model('Project', {
    'class': ProjectModel
  })
  .component('app',{
    'class': App,
    'env': '${env}',
    'dataSource': ioc.ref('dataSource'),
    'Project': ioc.ref('Project'),
    'User': ioc.ref('User')
  })
  .build({
    env: 'dev',
    server:{
      port: 9000
    },
    db:{
      database: 'restlib',
      username: 'restlib',
      password: 'restlib',
      sync: {force: true},//see http://docs.sequelizejs.com/en/latest/api/sequelize/#syncoptions-promise
      options: {
        host: 'localhost',
        port: '3306',
        logging: console.log
      }
    }
});




