const ioc = require('nan-ioc');
const restlib = require('rest-lib');

class Application {

  onInitialized() {
    this.expressServer
      .run()
      .then(() => console.log("Server running"));
  }
}

class HelloController {

  list() {
    console.log("Controller running");
    return "Hello world";
  }

}

class HelloPreMiddleware {
  get() {
    return (req, res, next) => {
      console.log("I'm running before controllers");
      next();
    }
  }
}

class HelloPostMiddleware {
  get() {
    return (req, res, next) => {
      console.log("I'm running after controllers");
      next();
    }
  }
}

ioc.module([restlib.core])
  .controller('helloController', {
    'class': HelloController,
    'context': '/hello'
  })
  .middleware('helloPreMiddleware', {
    'class': HelloPreMiddleware,
    'scope': 'application',
    'type': 'pre',
    'order': 1
  })
  .middleware('helloPostMiddleware', {
    'class': HelloPostMiddleware,
    'scope': 'application',
    'type': 'post',
    'order': 1
  })
  .component('application', {
    class: Application,
    expressServer: ioc.ref('expressServer')
  })
  .build({
    logger: {
      level: 'INFO'
    }
  });
